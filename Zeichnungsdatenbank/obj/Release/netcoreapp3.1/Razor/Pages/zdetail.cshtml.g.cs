#pragma checksum "C:\Users\Klaus\source\repos\Zeichnungsdatenbank\Zeichnungsdatenbank\Pages\zdetail.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "cd76fdbb9bcd441bfea31a6aa132a999ea77ca28"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(Zeichnungsdatenbank.Pages.Pages_zdetail), @"mvc.1.0.razor-page", @"/Pages/zdetail.cshtml")]
namespace Zeichnungsdatenbank.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Klaus\source\repos\Zeichnungsdatenbank\Zeichnungsdatenbank\Pages\_ViewImports.cshtml"
using Zeichnungsdatenbank;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"cd76fdbb9bcd441bfea31a6aa132a999ea77ca28", @"/Pages/zdetail.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"bab8c783e9d3a874d67a8d8f51b590b6eedf95d5", @"/Pages/_ViewImports.cshtml")]
    public class Pages_zdetail : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("href", new global::Microsoft.AspNetCore.Html.HtmlString("~/css/pure-min.css"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("rel", new global::Microsoft.AspNetCore.Html.HtmlString("stylesheet"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 3 "C:\Users\Klaus\source\repos\Zeichnungsdatenbank\Zeichnungsdatenbank\Pages\zdetail.cshtml"
  
    ViewData["Title"] = "Zeichnungshistorie";

#line default
#line hidden
#nullable disable
            WriteLiteral("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("link", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "cd76fdbb9bcd441bfea31a6aa132a999ea77ca284024", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n<div style=\"text-align: center;\">\r\n    <p>\r\n        <h1>Zeichnungshistorie</h1>\r\n    </p>\r\n    <p>");
#nullable restore
#line 13 "C:\Users\Klaus\source\repos\Zeichnungsdatenbank\Zeichnungsdatenbank\Pages\zdetail.cshtml"
  Write(ViewData["Meldung"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n    <p>\r\n        ");
#nullable restore
#line 15 "C:\Users\Klaus\source\repos\Zeichnungsdatenbank\Zeichnungsdatenbank\Pages\zdetail.cshtml"
   Write(ViewData["nummer"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("&nbsp;&nbsp;");
#nullable restore
#line 15 "C:\Users\Klaus\source\repos\Zeichnungsdatenbank\Zeichnungsdatenbank\Pages\zdetail.cshtml"
                                  Write(ViewData["Inhalt1"]);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
    </p>
    <table id=""dataTable"" class=""pure-table"" width=""100%"">
        <thead>
            <tr>
                <td>
                    Index
                </td>
                <td>
                    Status
                </td>
                <td>
                    Änderung
                </td>
                <td>
                    Datum
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    ");
#nullable restore
#line 37 "C:\Users\Klaus\source\repos\Zeichnungsdatenbank\Zeichnungsdatenbank\Pages\zdetail.cshtml"
               Write(ViewData["Index1"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
#nullable restore
#line 40 "C:\Users\Klaus\source\repos\Zeichnungsdatenbank\Zeichnungsdatenbank\Pages\zdetail.cshtml"
               Write(ViewData["Status1"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
#nullable restore
#line 43 "C:\Users\Klaus\source\repos\Zeichnungsdatenbank\Zeichnungsdatenbank\Pages\zdetail.cshtml"
               Write(ViewData["Änderungstext1"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
#nullable restore
#line 46 "C:\Users\Klaus\source\repos\Zeichnungsdatenbank\Zeichnungsdatenbank\Pages\zdetail.cshtml"
               Write(ViewData["modifiziert1"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n            </tr>\r\n            <tr class=\"pure-table-odd\">\r\n                <td>\r\n                    ");
#nullable restore
#line 51 "C:\Users\Klaus\source\repos\Zeichnungsdatenbank\Zeichnungsdatenbank\Pages\zdetail.cshtml"
               Write(ViewData["Index2"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
#nullable restore
#line 54 "C:\Users\Klaus\source\repos\Zeichnungsdatenbank\Zeichnungsdatenbank\Pages\zdetail.cshtml"
               Write(ViewData["Status2"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
#nullable restore
#line 57 "C:\Users\Klaus\source\repos\Zeichnungsdatenbank\Zeichnungsdatenbank\Pages\zdetail.cshtml"
               Write(ViewData["Änderungstext2"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
#nullable restore
#line 60 "C:\Users\Klaus\source\repos\Zeichnungsdatenbank\Zeichnungsdatenbank\Pages\zdetail.cshtml"
               Write(ViewData["modifiziert2"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    ");
#nullable restore
#line 65 "C:\Users\Klaus\source\repos\Zeichnungsdatenbank\Zeichnungsdatenbank\Pages\zdetail.cshtml"
               Write(ViewData["Index3"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
#nullable restore
#line 68 "C:\Users\Klaus\source\repos\Zeichnungsdatenbank\Zeichnungsdatenbank\Pages\zdetail.cshtml"
               Write(ViewData["Status3"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
#nullable restore
#line 71 "C:\Users\Klaus\source\repos\Zeichnungsdatenbank\Zeichnungsdatenbank\Pages\zdetail.cshtml"
               Write(ViewData["Änderungstext3"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
#nullable restore
#line 74 "C:\Users\Klaus\source\repos\Zeichnungsdatenbank\Zeichnungsdatenbank\Pages\zdetail.cshtml"
               Write(ViewData["modifiziert3"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n            </tr>\r\n            <tr class=\"pure-table-odd\">\r\n                <td>\r\n                    ");
#nullable restore
#line 79 "C:\Users\Klaus\source\repos\Zeichnungsdatenbank\Zeichnungsdatenbank\Pages\zdetail.cshtml"
               Write(ViewData["Index4"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
#nullable restore
#line 82 "C:\Users\Klaus\source\repos\Zeichnungsdatenbank\Zeichnungsdatenbank\Pages\zdetail.cshtml"
               Write(ViewData["Status4"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
#nullable restore
#line 85 "C:\Users\Klaus\source\repos\Zeichnungsdatenbank\Zeichnungsdatenbank\Pages\zdetail.cshtml"
               Write(ViewData["Änderungstext4"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
#nullable restore
#line 88 "C:\Users\Klaus\source\repos\Zeichnungsdatenbank\Zeichnungsdatenbank\Pages\zdetail.cshtml"
               Write(ViewData["modifiziert4"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n            </tr>\r\n            <tr>\r\n                <td>\r\n                    ");
#nullable restore
#line 93 "C:\Users\Klaus\source\repos\Zeichnungsdatenbank\Zeichnungsdatenbank\Pages\zdetail.cshtml"
               Write(ViewData["Index5"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
#nullable restore
#line 96 "C:\Users\Klaus\source\repos\Zeichnungsdatenbank\Zeichnungsdatenbank\Pages\zdetail.cshtml"
               Write(ViewData["Status5"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
#nullable restore
#line 99 "C:\Users\Klaus\source\repos\Zeichnungsdatenbank\Zeichnungsdatenbank\Pages\zdetail.cshtml"
               Write(ViewData["Änderungstext5"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
#nullable restore
#line 102 "C:\Users\Klaus\source\repos\Zeichnungsdatenbank\Zeichnungsdatenbank\Pages\zdetail.cshtml"
               Write(ViewData["modifiziert5"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n            </tr>\r\n        </tbody>\r\n    </table>\r\n</div>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Zeichnungsdatenbank.Pages.zdetailModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<Zeichnungsdatenbank.Pages.zdetailModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<Zeichnungsdatenbank.Pages.zdetailModel>)PageContext?.ViewData;
        public Zeichnungsdatenbank.Pages.zdetailModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
