﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Configuration;
using System.Web;
using System.Xml.Linq;
using Devart.Data;
using Devart.Data.MySql;


namespace Zeichnungsdatenbank.Pages
{
    public class zdetailModel : PageModel
    {


        public void OnGet()
        {

        }

        public void OnPost()
        {
            var pw = Request.Form["__token__"];
            var pid_neu = Request.Form["__pid_neu__"];
            var nummer_neu = Request.Form["__nummer_neu__"];

            MySqlConnectionStringBuilder myCSB = new MySqlConnectionStringBuilder();
            myCSB.Port = 3306;
            myCSB.Host = "www.schuermann.net";
            myCSB.UserId = "roerocadadmin";
            myCSB.Password = "Asd789qwe78r";
            myCSB.Database = "roerocad";
            myCSB.MaxPoolSize = 150;
            myCSB.ConnectionTimeout = 180;
            myCSB.Charset = "utf8";
            myCSB.LicenseKey = "pTm8cRkfpsx9iqw2deEZMS4VyWv//wNcA+ciXbcperM06hFNBdY14wf51XqKlhBmZNhPksTC5XU4YZDsYrH8QVVI3QxpNv326sf60RkAq8wDgyQCB8O/royDTQ082XhUK2cjaYLanFrqrIbIOZcOo6qgkIUEtQI9/4bN9DcpykbAE0xNUCLS5vXQj+5052uwR9S3uGRiiDD/b8PC/CfZ27vmIlrkNkO+NMlmHzFdfIPSHEe7YkJn8dlk+z3eMBLoRGIQn8jbYM+N5zDC5dqWSA==";

            DataTable dt1 = new DataTable();
            dt1.Columns.Add("ZeichnungsID", typeof(int));
            dt1.Columns.Add("ProjectID", typeof(int));
            dt1.Columns.Add("Nummer", typeof(string));
            dt1.Columns.Add("Inhalt", typeof(string));
            dt1.Columns.Add("Indexwert", typeof(string));
            dt1.Columns.Add("Aenderungstext", typeof(string));
            dt1.Columns.Add("Statuswert", typeof(string));
            dt1.Columns.Add("Bearbeiter", typeof(int));
            dt1.Columns.Add("modifiziert", typeof(string));

            try
            {
                MySqlConnection myConn = new MySqlConnection();
                myConn.ConnectionString = myCSB.ConnectionString;
                myConn.Open();
                MySqlCommand command = myConn.CreateCommand();
                command.CommandText = @"SELECT * FROM Zeichnungsliste WHERE ProjektID = " + pid_neu.ToString() + " AND Nummer = '" + nummer_neu + "' ORDER BY modifiziert DESC";
                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    dt1.Load(reader);

                }
                myConn.Close();
            }
            catch (Exception ex)
            {

            }

            int zaehler = 0;

            if (pw == "s50p40h33")
            {
                ViewData["nummer"] = nummer_neu;
                foreach (DataRow dr1 in dt1.Rows)
                {
                    if (zaehler == 0)
                    {
                        ViewData["Inhalt1"] = dr1["Inhalt"].ToString();
                        ViewData["Index1"] = dr1["Indexwert"].ToString();
                        ViewData["Änderungstext1"] = dr1["Aenderungstext"].ToString();
                        ViewData["Status1"] = dr1["Statuswert"].ToString();
                        ViewData["modifiziert1"] = dr1["modifiziert"].ToString();
                    }
                    if (zaehler == 1)
                    {
                        ViewData["Inhalt2"] = dr1["Inhalt"].ToString();
                        ViewData["Index2"] = dr1["Indexwert"].ToString();
                        ViewData["Änderungstext2"] = dr1["Aenderungstext"].ToString();
                        ViewData["Status2"] = dr1["Statuswert"].ToString();
                        ViewData["modifiziert2"] = dr1["modifiziert"].ToString();
                    }
                    if (zaehler == 2)
                    {
                        ViewData["Inhalt3"] = dr1["Inhalt"].ToString();
                        ViewData["Index3"] = dr1["Indexwert"].ToString();
                        ViewData["Änderungstext3"] = dr1["Aenderungstext"].ToString();
                        ViewData["Status3"] = dr1["Statuswert"].ToString();
                        ViewData["modifiziert3"] = dr1["modifiziert"].ToString();
                    }
                    if (zaehler == 3)
                    {
                        ViewData["Inhalt4"] = dr1["Inhalt"].ToString();
                        ViewData["Index4"] = dr1["Indexwert"].ToString();
                        ViewData["Änderungstext4"] = dr1["Aenderungstext"].ToString();
                        ViewData["Status4"] = dr1["Statuswert"].ToString();
                        ViewData["modifiziert4"] = dr1["modifiziert"].ToString();
                    }
                    if (zaehler == 4)
                    {
                        ViewData["Inhalt5"] = dr1["Inhalt"].ToString();
                        ViewData["Index5"] = dr1["Indexwert"].ToString();
                        ViewData["Änderungstext5"] = dr1["Aenderungstext"].ToString();
                        ViewData["Status5"] = dr1["Statuswert"].ToString();
                        ViewData["modifiziert5"] = dr1["modifiziert"].ToString();
                    }
                    zaehler++;
                }
                ViewData["meldung"] = "";
            }
            else
            {
                ViewData["meldung"] = "Falsches Passwort!";
            }
        }
    }
}