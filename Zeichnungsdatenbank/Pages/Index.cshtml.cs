﻿using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Devart.Data;
using Devart.Data.MySql;


namespace Zeichnungsdatenbank.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        public void OnGet(int checkid, int pid, string nummer)
        {
            int zid = 0;
            int pid_neu = 0;
            string nummer_neu = "";
            string tgnummer = "";
            string beschreibung = "";

            pid_neu = pid_neu + pid;
            nummer_neu = nummer_neu + nummer;

            MySqlConnectionStringBuilder myCSB = new MySqlConnectionStringBuilder();
            myCSB.Port = 3306;
            myCSB.Host = "www.schuermann.net";
            myCSB.UserId = "roerocadadmin";
            myCSB.Password = "Asd789qwe78r";
            myCSB.Database = "roerocad";
            myCSB.MaxPoolSize = 150;
            myCSB.ConnectionTimeout = 180;
            myCSB.LicenseKey = "pTm8cRkfpsx9iqw2deEZMS4VyWv//wNcA+ciXbcperM06hFNBdY14wf51XqKlhBmZNhPksTC5XU4YZDsYrH8QVVI3QxpNv326sf60RkAq8wDgyQCB8O/royDTQ082XhUK2cjaYLanFrqrIbIOZcOo6qgkIUEtQI9/4bN9DcpykbAE0xNUCLS5vXQj+5052uwR9S3uGRiiDD/b8PC/CfZ27vmIlrkNkO+NMlmHzFdfIPSHEe7YkJn8dlk+z3eMBLoRGIQn8jbYM+N5zDC5dqWSA==";

            string checktable = @"SELECT ZeichnungsID FROM Zeichnungsliste WHERE ProjektID = " + pid_neu.ToString() + " AND Nummer = '" + nummer_neu + "' ORDER BY modifiziert DESC Limit 1";

            try
            {
                MySqlConnection myConn = new MySqlConnection();
                myConn.ConnectionString = myCSB.ConnectionString;
                myConn.Open();
                MySqlCommand command = myConn.CreateCommand();
                command.CommandText = checktable;
                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    reader.Read();
                    zid = (int)reader.GetValue(0);
                }
                myConn.Close();
            }
            catch
            {

            }

            checktable = @"SELECT Projektnummer FROM Projektliste WHERE ProjektID = " + pid_neu.ToString() + " LIMIT 1";

            try
            {
                MySqlConnection myConn = new MySqlConnection();
                myConn.ConnectionString = myCSB.ConnectionString;
                myConn.Open();
                MySqlCommand command = myConn.CreateCommand();
                command.CommandText = checktable;

                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    reader.Read();
                    tgnummer = reader.GetValue(0).ToString();
                }
                myConn.Close();
            }
            catch
            {

            }

            checktable = @"SELECT Beschreibung FROM Projektliste WHERE ProjektID = " + pid_neu.ToString() + " LIMIT 1";

            try
            {
                MySqlConnection myConn = new MySqlConnection();
                myConn.ConnectionString = myCSB.ConnectionString;
                myConn.Open();
                MySqlCommand command = myConn.CreateCommand();
                command.CommandText = checktable;

                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    reader.Read();
                    beschreibung = reader.GetValue(0).ToString();
                }
                myConn.Close();
            }
            catch
            {

            }


            if (checkid == zid)
            {
                ViewData["header"] = "Zeichnung aktuell";
                ViewData["pic1"] = "green.png";
            }
            else
            {
                ViewData["header"] = "Zeichnung veraltet";
                ViewData["pic1"] = "red.png";
            }
            ViewData["tgnummer"] = tgnummer;
            ViewData["nummer"] = nummer_neu;
            ViewData["beschreibung"] = beschreibung;
            ViewData["test"] = checktable;
            ViewData["pid"] = pid_neu.ToString();

        }

        public void OnPost()
        {
            var pw = Request.Form["__token__"];
            ViewData["pw"] = pw;
        }

    }
}
